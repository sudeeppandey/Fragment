package pandey.sudeep.fragment;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private List<Integer> mylist;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView = cv;
        }
    }

    public Adapter(List<Integer> _lst){
        this.mylist = _lst;
    }

    @Override
    public int getItemCount(){
        return mylist.size();
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        CardView v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        final CardView cardView = holder.cardView;
        Button button = (Button)cardView.findViewById(R.id.button);
        button.setText( mylist.get(position).toString());
    }
}
