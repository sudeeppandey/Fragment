package pandey.sudeep.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements BlankFragment.OnFragmentInteractionListener, Fragment2.OnFragmentInteractionListener {

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(findViewById(R.id.button2)!=null){
            findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context,Main2Activity.class));
                }
            });
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri){

    }
}
